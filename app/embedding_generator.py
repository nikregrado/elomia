import numpy as np
from tqdm import tqdm
from transformers import TFRobertaModel, RobertaTokenizer
from emot.emo_unicode import UNICODE_EMOJI


class EmbeddingGenerator(object):
    def __init__(self) -> None:
        self.tokenizer = RobertaTokenizer.from_pretrained("roberta-large")
        self.transformer = TFRobertaModel.from_pretrained("roberta-large")

    def prepare_text(self, text):
        for emot in UNICODE_EMOJI:
            text = text.replace(emot, "_".join(UNICODE_EMOJI[emot].replace(",","").replace(":","").split()))
        return text

    def to_tokens(self, text):
        output = self.tokenizer.encode_plus(text, max_length=160, truncation=True, pad_to_max_length=True)
        return output

    def select_field(self, features, field):
        return [feature[field] for feature in features]

    def generate_embeddings(self, texts):
        list_of_embeddings = []
        for text in tqdm(texts):
            text = self.prepare_text(text)
            tokenizer_output = [self.to_tokens(text)]
            input_ids = np.array(self.select_field(tokenizer_output, 'input_ids'))
            attention_masks = np.array(self.select_field(tokenizer_output, 'attention_mask'))
            inputs = [input_ids, 
                attention_masks]
            embeddings = self.transformer.predict(inputs)
#         print(np.shape(embeddings[-1][-1]))
#         break
            list_of_embeddings.append(embeddings[0][0])
        return np.asarray(list_of_embeddings)   