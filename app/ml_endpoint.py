import tensorflow as tf

from flask import Flask, jsonify, request

from embedding_generator import EmbeddingGenerator


model = tf.keras.models.load_model('../model_weights/0_58_f_score.h5')
emb_gen = EmbeddingGenerator()


def predict_result(embeddings):
    return 1 if model.predict(embeddings)[0][0] > 0.5 else 0


app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def text_infer():
    
    content = request.json
    text_data = content['text']
    embs = emb_gen.generate_embeddings([text_data])
    result = predict_result(embs)
    return jsonify({"Need_to_calm_down": result})
    


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0', threaded=True)

