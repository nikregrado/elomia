Test task for Elomia team

Для написания эндпоинта был использован фласк, процессинг паралельных реквестов обеспечен за счет flask threading, то есть пошел самым простым путем

Как запустить:
pip install -r requirements.txt
cd app
python ml_endpoint.py

Flask сервер имеет один эндпоинт:
`http://"address"/predict`
Принимает один аргумент в теле запроса:
    - **text**: строка с месседжем юзера;
    Return:
    - JSON like: {"Need_to_calm_down": 1}. Need_to_calm_down в JSON может содержать следующие цифры - 1, 0. Если 1 то нужно дать упражнение, если 0 то не нужно

    Request/response example:
    ```JSON=True
    {
    "text": "I wanted to do my nails because just like 39 minutes ago I wanted to do my nails but I couldn't do it because I started crying and going all manic",
    }
    ```
    ```JSON=True
    {
    "Need_to_calm_down": 1
    }
    ```



В ноутбуке Modelling.ipynb краткое описание решения и то как я работал с моделью и данными
Данное тестовое было наверное самым необычным с чем я работал, за последний год, спасибо за интересное задание. Надеюсь мы еще посотрудничаем с вами